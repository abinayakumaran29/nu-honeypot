# Honeypot server , Detects and log connections
# Copyright ShieldLock(C) 2015 http://nucleon.shield-lock.co.il

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation version 2.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from time import strftime
from twisted.python.logfile import LogFile
from twisted.enterprise import adbapi
from twisted.python import log
import ConfigParser
import logging,sys

import conf
from p0f import P0f, P0fException

class p0fFactory:

    def __init__(self):

        self.conf = conf.ConfigFactory()
	
	if self.conf.p0fEnabled:
		self.p0f=P0f(self.conf.p0fsocket)
		print 'p0f connected'


    def getIp(self, ip):
	try:
	  data = self.p0f.get_info(ip)
          return data
	except P0fException, e:
    	# Invalid query was sent to p0f. Maybe the API has changed?
    		return e
	except KeyError, e:
    	# No data is available for this IP address.
   		return e
	except ValueError, e:
    # p0f returned invalid constant values. Maybe the API has changed?
	        return e

