# Honeypot server , Detects and log connections
# Copyright ShieldLock(C) 2015 http://nucleon.shield-lock.co.il

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation version 2.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from time import strftime
from twisted.python.logfile import LogFile
import MySQLdb
from twisted.python import log
import ConfigParser
import logging,sys
import reporter

class ConfigFactory:

    def __init__(self):


	config = ConfigParser.ConfigParser()
	config.read('honeypot.cfg')
	dbname=config.get('MySQL','dbname')
	dbuser=config.get('MySQL','dbuser')
	dbpass=config.get('MySQL','dbpass')
	dbhost=config.get('MySQL','dbhost')
	self.dbenabled=config.getboolean('MySQL','enabled')
	self.telnetEnabled = config.get('Telnet','enabled')
	self.pop3Enabled = config.get('POP3','enabled')
	self.nucleonEnabled = config.get('Reporting','nucleon')
	self.p0fEnabled = config.get('p0f','enabled')
	self.pop3ServerPort = config.getint('POP3','service_port')
	self.telnetServerPort = config.getint('Telnet','service_port')
	self.p0fsocket = config.get('p0f','socket')
        self.nureports = config.get('Reporting','nucleon')
	self.dbpool=False

	self.logger  = logging.getLogger('Telnet')
	self.logger.addHandler(logging.StreamHandler(sys.stderr))
	#logger.addHandler(logging.FileHandler(logfile))
	for h in self.logger.handlers:
    		h.setFormatter(logging.Formatter(fmt='%(asctime)s|[%(name)s.%(levelname)s %(lineno)d]:| %(message)s'))

	self.logger.setLevel(logging.DEBUG)

	if self.dbenabled:
	        print "*"
		self.db = MySQLdb.connect(host=dbhost,
                     user=dbuser,
                     passwd=dbpass,
                     db=dbname)

        	self.cur = self.db.cursor()
    

    def log2db(self, sql):
	if self.db:
        	d = self.cur.execute(sql)
		self.db.commit()
		print "report is %s" % d

	if self.nureports:
		r = reporter.ReporterFactory()
		r.sendReport(sql)
