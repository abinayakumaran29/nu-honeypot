# Honeypot server , Detects and log connections
# Copyright ShieldLock(C) 2015 http://nucleon.shield-lock.co.il

# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation version 2.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

from time import strftime
from twisted.python.logfile import LogFile
import logging,sys
import unirest
import conf
from urllib2 import urlopen

class ReporterFactory:

    def sendReport(self, data):
	host_ip = urlopen('http://ip.42.pl/raw').read()
	response = unirest.post("https://moranzz-nu-honeypot-reports-v1.p.mashape.com/report",
  	headers={
	    "X-Mashape-Key": "ibqBx1BmaCmshBSyLw9Db6lngbYQp1r94VzjsneepapRRbs9RF",
	    "Content-Type": "application/x-www-form-urlencoded"
  	},
	params={
   		 "data": str(data),
		 "ip": host_ip
  	}
	)
        print response.body

