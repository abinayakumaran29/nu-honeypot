# README #


Python Honeypot supporting multiple protocols.

[Make sure to read documentation](http://honeypot.readme.io)

[Installation is easy if you follow the instructions](https://honeypot.readme.io/docs/install)

                          __.--'~~~~~`--.
         ..       __.    .-~               ~-.
         ((\     /   `}.~                     `.
          \\\  .{     }               /     \   \
      (\   \\~~       }              |       }   \
       \`.-~ -@~     }  ,-,.         |       )    \
       (___     ) _}  (    :        |    / /      `._
        `----._-~.     _\ \ |_       \   / /-.__     `._
               ~~----~~  \ \| ~~--~~~(  + /     ~-._    ~-._
                         /  /         \  \          ~--.,___~_-_.
                      __/  /          _\  )
                    .<___.'         .<___/    "Triceratops"